CUDA_CC = nvcc
CUDA_FLAGS = -arch=sm_20 -m64 -G -g

BIN = a.out

all: $(BIN)

a.out: measure-time.cu
	$(CUDA_CC) $(CUDA_FLAGS) -o $@ $<

clean:
	$(RM) $(BIN)
