/*
 * Author: hadrihilmi@gmail.com
 * File: measure-time.cu
 *
 * Description: Simple way to measure time + performance */

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#define BLOCKS_PER_GRID 1
#define THREADS_PER_BLOCK 1024

__device__ void func1() {
	printf("hello from device function: func1\n");
}

__global__ void kernel(int *d_ptr) {
	int idx = threadIdx.x + blockDim.x * blockIdx.x;

	if(idx < 10) {
		printf("hello from kernel\n");
		func1();
	}
}

int main(int argc, char *argv[]) {

	// set device: Tesla
	cudaSetDevice(0);

	// cuda event properties
	cudaError_t error;
	cudaEvent_t start, stop;

	// alloc cuda start & stop events
	error = cudaEventCreate(&start);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to create start event (error code: %s)\n", cudaGetErrorString(error));

	error = cudaEventCreate(&stop);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to create stop event (error code: %s)\n", cudaGetErrorString(error));

	// var init
	int *h_ptr, *d_ptr;

	// mem alloc
	size_t cudaSize = THREADS_PER_BLOCK * sizeof(int);
	h_ptr = (int*) calloc(THREADS_PER_BLOCK, sizeof(int));
	cudaMalloc((void**) &d_ptr, cudaSize);

	// put some data
	int i;
	for(i = 0; i < THREADS_PER_BLOCK; i++) {
		h_ptr[i] = 12;
	}

	// record start event ----------------------------------------------------------------------------------------------
	error = cudaEventRecord(start, NULL);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to record start event (error code: %s)\n", cudaGetErrorString(error));

	// copy host-to-device
	cudaMemcpy(d_ptr, h_ptr, cudaSize, cudaMemcpyHostToDevice);

	// record stop event
	error = cudaEventRecord(stop, NULL);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to record stop event (error code: %s)\n", cudaGetErrorString(error));

	// sync stop event
	error = cudaEventSynchronize(stop);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to sync between events (error code: %s)\n", cudaGetErrorString(error));

	// cal elapsed time
	float msecTotal = 0.0f;
	error = cudaEventElapsedTime(&msecTotal, start, stop);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to get time elapsed between events (error code: %s)\n", cudaGetErrorString(error));
	
	// print out elapsed time
	float bandwidthInMBs = 0.0f;
	bandwidthInMBs = (1e3f * cudaSize) / (msecTotal * (float)(1 << 20));
	printf("Performance copy Host-To-Device %lu Bytes>>> Elapsed time = %.4f msec\t Bandwidth = %.2f MB/s\n", cudaSize, msecTotal, bandwidthInMBs);

	// launch kernel ---------------------------------------------------------------------------------------------------
	kernel<<< BLOCKS_PER_GRID, THREADS_PER_BLOCK >>>(d_ptr);

	error = cudaEventRecord(start, NULL);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to record start event (error code: %s)\n", cudaGetErrorString(error));

	// copy device-to-host
	cudaMemcpy(h_ptr, d_ptr, cudaSize, cudaMemcpyDeviceToHost);

	// record stop event
	error = cudaEventRecord(stop, NULL);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to record stop event (error code: %s)\n", cudaGetErrorString(error));

	// sync stop event
	error = cudaEventSynchronize(stop);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to sync between events (error code: %s)\n", cudaGetErrorString(error));

	// cal elapsed time
	msecTotal = 0.0f;
	error = cudaEventElapsedTime(&msecTotal, start, stop);

	if(error != cudaSuccess)
		fprintf(stderr, "Failed to get time elapsed between events (error code: %s)\n", cudaGetErrorString(error));
	
	// print out elapsed time
	bandwidthInMBs = 0.0f;
	bandwidthInMBs = (1e3f * cudaSize) / (msecTotal * (float)(1 << 20));
	printf("Performance copy Device-To-Host %lu Bytes>>> Elapsed time = %.4f msec\t Bandwidth = %.2f MB/s\n", cudaSize, msecTotal, bandwidthInMBs);

	// verify the data --------------------------------------------------------------------------------------------------

	// clean up memory
	free(h_ptr);
	cudaFree(d_ptr);

	printf("\nMAX Tesla (device: 0) Bandwidth = %.1f GB/sec\n", (115 * 1e9 * (384/8) * 2) / 1e9);

	return 0;
}
